import os
from setuptools import setup

from pystat import version

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "pystat",
    version = version,
    author = "Tordjman Laurent",
    author_email = "laurent.tordjman@orange.com",
    description = ("A command line tool to transform logs from robotframework"),
    license = "orange",
    keywords = "robotframework logs statistics",
    url = "",
    packages=['pystat'],
    #scripts=['bin/pyqc.py'],
    entry_points = {
        'console_scripts': ['pystat=pystat.main:main'],
    },
    long_description=read('README.md'),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: Orange License",
    ],
)
