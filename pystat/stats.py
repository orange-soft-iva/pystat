__author__ = 'cocoon'
"""

    produce stat files yaml

"""


import yaml
import json
import copy

from matrix import Table2D




class StatBase(object):
    """

    """
    _kind = "base"

    @property
    def kind(self):
        return self._kind

    @property
    def name(self):
        return self._name

    @property
    def meta(self):
        return self._data['_meta']


    @property
    def data(self):
        """

        :return:
        """
        res = { self.name : self._data}
        return res


    def to_json(self):
        """

        :return:
        """
        data = json.dumps(self.data,indent=4)
        return data



class StatHeader(StatBase):
    """

	OVC:
		_meta: {}
		tests:
			- ModifyCompany
			- ModifyMeetingBooking
		envs:
			- Chrome32_XP
			- IE7_XP
			- ff25_XP
		keywords:
			- "Selenium2Library.Click Element sub_menu_label_3"
			- "Selenium2Library.Wait Until Page Contains liste des salles, timeout=${timeGig}"
			- "Selenium2Library.Click Element xpath=(//label[@class='rightButtonPanel rightButtonSmallSize noImageSmallLightGreyButtonUnSelectedRightPanel'])[1]"


    """
    _kind = "headers"


    def __init__(self,name="job" , data={}, meta={}):
        """

        :param name:
        :param data:
        :return:
        """
        self._name = name

        if not data:
            data = {
                '_meta': meta,
                'tests': set(),
                'envs' : set(),
                'keywords': set()

            }
        self._data = data

    @property
    def tests(self):
        """

        :return:
        """
        return self._data['tests']

    @property
    def envs(self):
        """

        :return:
        """
        return self._data['envs']

    @property
    def keywords(self):
        """

        :return:
        """
        return self._data['keywords']


    @property
    def data(self):
        """

        :return:
        """

        data = { self.name : {
            '_meta': self.meta ,
            'tests': sorted(list(self._data['tests'])),
            'envs': sorted(list(self._data['envs'])),
            'keywords': sorted(list(self._data['keywords'])),
            }
        }
        return data





class StatRun(StatBase):
    """

        1:
            "_meta": {}
            "test_x_env":
                ModifyCompany:
                    Chrome32_XP: 1
                ModifyMeetingBooking:
                    Chrome32_XP: 0
            "env_x_keywords":
                Chrome32_XP:
                    0: # "Selenium2Library.Click Element sub_menu_label_3"
                        1

    """
    _kind = 'run'


    def __init__(self,name=0 , meta=None, data=None):
        """

        :param name:
        :param data:
        :param meta:
        :return:
        """
        self._name = name
        if not meta:
            meta={}

        if not data:
            data = {
                '_meta': meta,
                'env_x_keyword' : Table2D(labels=('env','keyword')),
                'env_x_test' : Table2D(labels=('env','test')),

            }
        self._data = data

    @property
    def data(self):
        """

        :return:
        """

        data = { self.name : {
            '_meta': self.meta ,
            'env_x_keyword': self._data['env_x_keyword'].data,
            'env_x_test': self._data['env_x_test'].data ,
            }
        }
        return data


    def copy(self):
        """

        :return: a copy of itself
        """
        twin = StatRun(name=self.name,data=copy.deepcopy(self._data))
        return twin

    def table(self,table_name):
        """


        :param table_name: string eg env_x_test ...
        :return:
        """
        return self._data[table_name]

    def iter_tables(self):
        """

        :return:
        """
        for name, table in self._data.iteritems():
            if name.startswith('_'):
                # a special name: skip
                continue
            yield name , table

    def inc(self,test_name,env_name,kw_name,value=1):
        """
            add a result to the run

        :param test_name:
        :param env_name:
        :param kw_name:
        :return:
        """
        #add to test_x_env
        #self._data['test_x_env'].inc(test_name,env_name,value)
        # add to env_x_keywords
        self._data['env_x_keyword'].inc(env_name,kw_name,value)
        # add to env_x_test
        self._data['env_x_test'].inc(env_name,test_name,value)
    add = inc



    def merge(self,*runs):
        """
            merge intra run

        :param runs: array of StatRun instance to merge
        :return: a new merged run
        """
        merged =self.copy()
        for run in runs:
            if run.name != merged.name:
                # not the same run cant merge
                raise ValueError("not the same run , cant merge")
            for name, table in merged.iter_tables():
                # find the equivalent table in the run to merge
                table_to_add = run.table(name)
                # merging
                merged._data[name] = merged.table(name).merge(table_to_add)

        return merged

    @classmethod
    def from_data(cls,data):
        """
            build a run object from data

        :param data:
        :return:
        """
        # check structure
        assert isinstance(data,dict)
        assert len (data.keys()) == 1
        run_name = data.keys()[0]
        rub = data[run_name]
        assert isinstance(rub,dict)
        meta = rub['_meta']
        run = cls(run_name,meta=meta)

        # build test_x_env
        #run._data['test_x_env']= Table2D(labels=('test','env'),data=rub['test_x_env'])
        run._data['env_x_keyword'] = Table2D(labels=('env','keyword') ,data=rub['env_x_keyword']  )
        run._data['env_x_test'] = Table2D(labels=('env','test') ,data=rub['env_x_test']  )
        return run


    def to_yaml(self,indent=0,spaces=4):
        """

        :param indent:
        :param spaces:
        :return:
        """

        space = " " * spaces

        i0 = space * indent
        i1 = space * (1+indent)

        lines=[]
        lines.append("%s%s: " % (i0,str(self.name)))
        lines.append("%s_meta: %s" % (i1, str(self.meta)) )

        # lines.append("%stest_x_env :" % i1  )
        # tab1 = self._data['test_x_env'].to_yaml(indent+2)
        # lines.append(tab1)


        lines.append("%senv_x_test :" % i1  )
        tab = self._data['env_x_test'].to_yaml(indent+2)
        lines.append(tab)

        lines.append("%senv_x_keyword :" % i1  )
        tab2 = self._data['env_x_keyword'].to_yaml(indent+2)
        lines.append(tab2)

        lines="\n".join(lines)

        return lines






if __name__=="__main__":



    def test_stat_run():


        meta = {'doc':"th doc", 'name':"my name"}

        run = StatRun(1,meta=meta)
        run.add('modifyCompany','Chrome32_XP','click')
        run.add('modifyCompany','IE7_XP','click')
        run.add('ModifyMeetingBooking','Chrome32_XP','click')
        run.add('ModifyMeetingBooking','IE9_XP','click')


        print run.to_json()

        run_data = run.data



        new_run = StatRun.from_data(run_data)


        yaml = new_run.to_yaml(2)

        print yaml
        return


    def test_stat_header():
        """

        :return:
        """
        h = StatHeader('OVC')


        for t in ['modifyCompany','ModifyMeetingBooking']:
            h.tests.add(t)

        for e in ['Chrome32_XP','IE9_XP']:
            h.envs.add(e)

        for k in ['click1','click2']:
            h.keywords.add(k)
        return


    def test_merge_runs():
        """


        :return:
        """
        a = StatRun( 1 )
        a.add('t1','e1','k1')
        a.add('t2','e1','k2')

        b = StatRun( 1 )
        b.add('t1','e1','k1')
        b.add('t1','e2','k1')
        b.add('t3','e2','k3')


        c=a.merge(b,)


        print "stat run a:"
        print "---"
        print a.to_yaml()

        print "stat run c:"
        print "---"
        print b.to_yaml()

        print "stat run c  (a+b):"
        print "---"
        print c.to_yaml()



        return


    def test_load_from_data():

        # load result  OVC/150/Chrome32WINXP
        with open(base + "/OVC/150/Chrome32WINXP/stat_errors.yaml"  ,"rb") as fh:
            data = yaml.load(fh)
            e1 = StatRun.from_data(data)


        # load result  OVC/150/FF24WIN7


        return



    def test():


        test_load_from_data()

        test_merge_runs()

        test_stat_run()
        test_stat_header()
        return




    ########
    base="../samples/var/robot/results"




    test()