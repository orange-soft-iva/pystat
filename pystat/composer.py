__author__ = 'cocoon'
"""



"""

import os
from copy import copy , deepcopy

from StringIO import StringIO

from xml.etree import cElementTree as ET


from robot import rebot


from scanoutput import scan_errors, Screenshot , find_all_test
from stats import StatRun

def copy_last_kw(kw):
    """

    copy a the complete keyword ( with snapshoit)

    :param kw:
    :return:
    """
    new = deepcopy(kw)
    return new



def copy_kw(kw,debug=False):
    """

    copy kw with all chidren exxcept kw

    <kw type= name=
        <doc
        <kw *
        <arguments
            <arg *
        <status


    :param kw:
    :return:
    """
    #make a base clone
    new = ET.Element(kw.tag,**kw.attrib)
    new.text = kw.text

    # add doc , arguments , status
    for name in ['doc','arguments','status']:
        new.append(deepcopy(kw.find(name)))

    if debug:
        print ET.tostring(new)

    return new




def copy_test(tst,debug=False):
    """

    copy test with all but kw

    <test id= name=

        <kw *
        <doc
        <tags

        <status



    :param tst:
    :return:
    """
    #make a base clone
    new = ET.Element(tst.tag,**tst.attrib)
    new.text = tst.text

    # add doc , arguments , status
    for name in ['doc','tags','status']:
        new.append(deepcopy(tst.find(name)))

    if debug:
        print ET.tostring(new)

    return new





def copy_suite(suite,debug=False):
    """

    copy all but source and suite

    <suite source= id= name=
        <suite *
        <test *
        <doc
        <metadata
        <status


    :param suite:
    :return:
    """
    #make a base clone
    new = ET.Element(suite.tag,**suite.attrib)
    new.text = suite.text

    # add doc , arguments , status
    for name in ['doc','metadata','status']:
        new.append(deepcopy(suite.find(name)))

    if debug:
        print ET.tostring(new)

    return new


def copy_root(root,debug=False):
    """
        copy all but suite

    <robot generated="20140627 22:27:12.570" generator="Robot 2.8.1 (Python 2.7.3 on linux2)"
        <suite *
        <statistics
        <errors

    :param root:
    :return:
    """
    #make a base clone
    new = ET.Element(root.tag,**root.attrib)
    new.text = root.text

    # add doc , arguments , status
    for name in ['statistics','errors']:
        new.append(deepcopy(root.find(name)))

    if debug:
        print ET.tostring(new)

    return new


class TestImage(object):
    """
        xml image of a test on error

        test_element
        array of kw_elements

    """

    def __init__(self,test,keywords):
        """
            make the xml image of the test
        """
        self._test =test
        self._keywords = keywords

        self._snapshots = []



    def make(self,debug=False):
        """




        """

        if len(self._keywords)== 0:
            raise ValueError("TestImage.make: keywords list is empty")

        # get last keyword on error
        self.last_keyword=self._keywords[-1]
        botom= self._keywords[-1]
        if self.is_snapshot(botom):
            # last element is a snapshot dont search for other children
            pass
        else:
            # try to find a child keyword of bottton
            child_kw= botom.find('./kw')
            if child_kw :
                if self.is_snapshot(child_kw):
                    # there is a snapshot
                    snapshot = Screenshot( child_kw)
                    self._snapshots.append(snapshot.snapshot)
                    # add this keyword
                    snapshot_node= copy_kw(child_kw,debug=debug)
                    snapshot_node.append(deepcopy(child_kw.find('msg')))
                    #snapshot_node.append(deepcopy(child_kw.find('msg')))
                    #snapshot_node.text= snapshot.msg.text
                    #snapshot_node.append(snapshot.msg)

                    # add snapshot keyword
                    #snapshot_node= child_kw
                    self._keywords.append(snapshot_node)
                else:
                    if debug:
                         print("no snapshot keywords found")
                    pass
            else:
                # no child keyword
                pass


        new_test = copy_test(self._test)
        for kw in (self._keywords):
            if debug:
                print kw.tag, kw.attrib

            new = copy_kw(kw)
            if self.is_snapshot(new):
                new.append(deepcopy(kw.find('msg')))


            new_test.append(new)

            if debug:
                 print ET.tostring(new)

        return new_test


        # new_test = copy_test(self._test)
        #
        # if not new:
        #     raise ValueError("new is empty")
        #
        #
        # new_test.append(new)
        #
        # if debug:
        #      print ET.tostring(new_test,method='xml')
        #
        #
        # return new_test




        # # extract the last keyword on error
        # last = self._keywords.pop(-1)
        # if self.is_snapshot(last):
        #     # last bad keyword is itself a snapshot
        #     new_test = copy_test(self._test)
        #     new = copy_kw(last)
        #     new_test.append(new)
        #     return new_test
        #
        #
        # self.last_keyword = last
        #
        # # extract the snapshot xml kw if any
        # child_kw= last.find('./kw')
        # if child_kw :
        #     if child_kw.attrib['name']=="Selenium2Library.Capture Page Screenshot":
        #         snapshot = Screenshot( child_kw)
        #         self._snapshots.append(snapshot.snapshot)
        #     else:
        #         if debug:
        #              print("no snapshot keywords found")
        #         pass
        # else:
        #     # no child keyword
        #     if debug:
        #         print("no child keyword found")
        #     pass
        #     return
        #
        # new_last = copy_last_kw(last)
        #
        #
        # if debug:
        #     print ET.tostring(new_last)
        #
        # last= new_last
        #
        # new= None
        #
        # for kw in reversed(self._keywords):
        #     if debug:
        #         print kw.tag, kw.attrib
        #
        #     new = copy_kw(kw)
        #
        #     new.append(last)
        #
        #     last = new
        #
        #
        #     if debug:
        #          print ET.tostring(new)
        #
        # new_test = copy_test(self._test)
        #
        # if not new:
        #     raise ValueError("new is empty")
        #
        #
        # new_test.append(new)
        #
        # if debug:
        #      print ET.tostring(new_test,method='xml')
        #
        #
        # return new_test

    @property
    def name(self):
        """
            return the name of the test
        """
        return self._test.attrib['name']

    @property
    def keyword(self):
        """
            return name of the last keyword
        """
        return self.last_keyword.attrib['name']

    def keyword_args(self):
        """
            return array of args of last keywords
        """
        arg_elements = self.last_keyword.findall('./arguments/arg')

        args = [ arg.text for arg in arg_elements]

        return args

    @property
    def full_keyword(self):
        """
        """
        return self.keyword + ' ' +  " ".join(self.keyword_args())

    @classmethod
    def is_snapshot(self,element):
        """

        """
        if element.tag == 'kw':
            if element.attrib['name']=="Selenium2Library.Capture Page Screenshot":
                return True
        return False

class SuiteImage(object):
    """

    """
    def __init__(self,root):
        """
        :root: element
        """
        self.root = root    # root element of the scanned output.xml

        # update by make
        self.test_stack = [] # array of TestImage for test on errors
        self.snapshots = []  # array of snapshot filenames of the underlying tests

        self.xml_tests =[]  # list of all test of the suite (liste of xml elements


    def testname_list(self):
        """
        """
        names =[]
        for tst in find_all_test(self.root):
            names.append(tst.attrib['name'])
        return names

    def make(self,debug=False):
        """

        """
        root = self.root
        last_test = None
        keywords=[]

        test_stack=[]
        for kind, element  in scan_errors(root,debug=debug):
            if kind == "TEST":
                if debug:
                    print ">> bad test :%s " % element.attrib['name']

                if last_test:
                    # end of preceding test
                    t = TestImage(last_test,keywords)
                    test_stack.append(t)
                # save last test
                last_test = element
                keywords= []

            else:
                if debug:
                    print ">>> bad keyword: %s" % element.attrib['name']
                keywords.append(element)

        # treat last test
        if keywords:
            # at least one keyword is on error
            t = TestImage(last_test,keywords)
            test_stack.append(t)

            self.test_stack = test_stack


            new_stack =[]
            for t in test_stack:

                new_test = t.make(debug=debug)
                self.snapshots.extend(t._snapshots)
                new_stack.append(new_test)

            suite = root.find('./suite')
            new_suite = copy_suite(suite)
            for t in new_stack:
                new_suite.append(t)

            new_root = copy_root(root)
            new_root.append(new_suite)

            if debug:
                print ET.tostring(new_root)

            return new_root
        else:
            # no keywords on error
            return None



class RobotImage(object):
    """
        image of a robot framework output.xml
    """
    def __init__(self, path , filename='output.xml'):
        """
        """
        self.filename = filename
        self.base = path

        self.filepath = os.path.abspath(os.path.join(self.base,self.filename))

        # get meta info from path ( job, run , env )
        rep , f = os.path.split(self.filepath)
        paths= rep.split(os.path.sep)
        self._meta= {
            'job': paths[-3],
            'run': paths[-2],
            'env': paths[-1]
        }

        self.snapshots=[]


    def meta(self, property = None):
        """

        :return:
        """
        if property is None:
            # return the dict
            return self._meta
        else:
            # return the given property
            return self._meta[property]

    def make(self,to_string=False,debug= False):
        """
            make the xml representation of new output.xml
        """
        self.xml = ET.parse(self.filepath)

        self.root = self.xml.getroot()


        self.suite = SuiteImage(self.root)

        self.xml_out = self.suite.make(debug=debug)

        if self.xml_out:
            self.snapshots = self.suite.snapshots

            if debug:
                print ET.tostring(self.xml_out)

            if to_string:
                # return string
                return ET.tostring(self.xml_out)
            else:
                # return element
                return self.xml_out
        else:
            # no errors
            return None




    def stat(self,job=None,run=None, env= None):
        """

            build the stat for this job/run/env


        :param job:
        :param run:
        :param env:
        :return:
        """
        if job is None:
            job=self.meta("job")
        if run is None:
            run= int(self.meta('run'))
        if env is None:
            env = self.meta('env')


        #report = YamlReport('OVC')
        report = YamlReport(job)

        stat_run = StatRun(run,meta=self.meta())

        tests = self.suite.test_stack
        for t in tests:
            #print t.name
            #print t.keyword
            #print t.full_keyword

            #print t.keyword_args()
            stat_run.add(t.name,self.meta('env'),t.full_keyword)


        #yaml = stat_run.to_yaml()

        report.add_run(stat_run)

        yaml = report.to_yaml()



        return yaml



    def rebot(self, directory="./", output_filename="output_errors.xml", report='report_errors.html',
              log='log_errors.html'):
        """

            generate
                - output_errors.xml
                - output_errors.txt
                - report_errors.html
                - log_errors.html

        :param directory:
        """
        output_filename = os.path.join(directory, output_filename)
        report = os.path.join(directory, report)
        log = os.path.join(directory, log)

        txt_filename = os.path.join(directory, 'output_errors.txt')

        # make output file
        #with open(output_filename,"w") as fh:
        #    fh.write(ET.tostring(self.xml_out))

        source = output_filename
        with open(txt_filename,'w') as stdout:
            rebot(source,report=report,log=log,stdout=stdout)

        return



class YamlReport(object):
    """


        jobs:
            OVC:
                #_meta: {}
                _meta: {'doc': 'th doc', 'name': 'my name'}
                tests:
                    - ModifyCompany
                    - ModifyMeetingBooking
                envs:
                    - Chrome32_XP
                    - IE7_XP
                    - ff25_XP
                keywords:
                    - "Selenium2Library.Click Element sub_menu_label_3"
                    - "Selenium2Library.Wait Until Page Contains liste des salles, timeout=${timeGig}"
                    - "Selenium2Library.Click Element xpath=(//label[@class='rightButtonPanel rightButtonSmallSize noImageSmallLightGreyButtonUnSelectedRightPanel'])[1]"

                runs:
                    1:
                        "_meta": {}
                        "test_x_env":
                            ...


    """
    def __init__(self,job='job'):
        """

        """
        self.job = job
        self._runs = []

    def add_run(self,stat_run):
        """
        """
        self._runs.append(stat_run)


    def _merge_runs(self,runs):
        """
        :runs: array of Statrun instances
        """
        if len(runs) == 1:
            merged = runs[0]
        else:
            merged = runs[0].merge(runs[1:])
        return merged


    def to_yaml(self):
        """
        """
        lines = []

        lines.append("jobs:")
        lines.append("    %s:" % self. job )
        lines.append("        runs:" )

        indent = 3

        runs = self._merge_runs(self._runs)
        run = runs.to_yaml(indent = indent)

        lines.append(run)

        return "\n".join(lines)




if __name__ == "__main__":
    """
    """


    from scanoutput import scan_errors,treat_test

    base = "../../samples/var/robot/results"

    o1 = "%s/%s" % (base,"OVC/150/Chrome32WINXP/output.xml")



    test_stack = []



    def test():


        output = RobotImage("../../samples/var/robot/results/OVC/150/Chrome32WINXP")




        xml = output.make(debug= True)

        stat = output.stat()

        print stat

        with open("stat_output.yaml","w") as fh:
            fh.write(stat)




        print output.suite.snapshots
        print output.suite.testname_list()

        output.rebot()



        return





    #####
    test()


