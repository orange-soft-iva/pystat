__author__ = 'cocoon'
"""



    <test id= name=

        <kw *
        <doc
        <tags

        <status


    <kw type= name=
        <doc
        <kw *
        <arguments
            <arg *
        <status


    <status status= endtime="20140627 22:31:36.748" critical="yes" starttime="20140627 22:29:07.830"
        _text

    <suite source= id= name=
        <suite *
        <test *
        <doc
        <metadata
        <status


    <robot generated="20140627 22:27:12.570" generator="Robot 2.8.1 (Python 2.7.3 on linux2)"
        <suite *
        <statistics
        <errors



"""
import re
from xml.etree import cElementTree as ET




class Screenshot(object):
    """

        a keyword for a snapshot ( xml element )


	<kw type="kw" name="Selenium2Library.Capture Page Screenshot">
		<doc>Takes a screenshot of the current page and embeds it into the log.</doc>
		<arguments>
		</arguments>
		<msg timestamp="20140627 22:31:05.598" html="yes" level="INFO">&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td colspan="3"&gt;&lt;a href="selenium-screenshot-6.png"&gt;&lt;img src="selenium-screenshot-6.png" width="800px"&gt;&lt;/a&gt;</msg>
		<status status="PASS" endtime="20140627 22:31:05.598" starttime="20140627 22:31:05.158"></status>
	</kw>


    """
    # extract href
    pattern = re.compile(r'href="(.*?)"')

    def __init__(self,xml_kw):
        """

        """
        self.element = xml_kw

        self.treat()

    def treat(self):
        """
        """
        assert self.element.attrib['type'] == "kw"
        assert self.element.attrib['name'] == "Selenium2Library.Capture Page Screenshot"

        self.msg = self.element.find('msg')
        #print msg.attrib['timestamp']
        #print msg.text

        match = self.pattern.search(self.msg.text)
        if match:
            self.snapshot = match.group(1)

        return


def find_all_test(start,debug=True):
    """
        iter on each test of a suite

    :param start: element
    :return: iterator
    """
    # iter on each test
    for t in start.iter('test'):
        if debug >= 1:
            print t.tag , t.attrib , t.attrib['name']
        yield t



def find_all_failed_test(start,debug=0):
    """
        from start , yield each test with status == FAIL

    :param start:
    :return:
    """
    # iter on each test
    for t in start.iter('test'):
        if debug >= 1:
            print t.tag , t.attrib , t.attrib['name']

        # find test status
        status = t.find('./status')
        #print status.tag ,status.attrib

        if status.attrib['status'] == 'FAIL':
            # this test has failed
            if debug > 0:
                print ">> bad test :%s " % t.attrib['name']
            yield  t



def find_first_failed_child_keyword(kw,debug=False):
    """

        look for immediate children of kw , return first keyword on error

    :param kw:element , start keyword
    :return: kw on error or 0 or None
    """
    hits = 0
    while 1:

        for e in kw:

            hits += 1
            # for each direct child
            if e.tag == 'kw' :
                # this is a keyword child
                if True:
                #if e.attrib['type'] == 'kw' or e.attrib['type'] == 'setup':
                    # it is a keyword of type kw :
                    kw_status =  e.find('./status')
                    #print kw_status.tag, kw_status.attrib

                    #if kw_status.attrib['status'] == 'PASS':
                    if kw_status.attrib['status'] == 'FAIL':
                        # we found a failed keyword
                        if debug:
                            print ">>> bad keyword: %s" % e.attrib['name']
                        return e

            # in other case just ignore
            #  - not a keyword
            #  - not keyword of type 'kw'
            #  - keyword of type 'kw' with status not FAIL
            pass

        # no child on errors
        if hits == 0 :
            # origin kw has no children
            return 0
        else:
            # origin keyword has children, but none on errors
            return None


def scan_errors(start,debug=False):
    """
        generator for test and keyword chain on error

        from start
           find test on error and yield ('TEST', test_element)
                for each kw on error of test
                    yield ( 'KEYWORD' , kw_element)



    :param start:element to start search
    :return:
    """
    #tests = list(find_all_failed_test(start))

    tests = find_all_failed_test(start)
    for t in tests:
        if debug:
            print t.tag,t.attrib
        yield 'TEST' , t

        #failed_keywords = []
        kw = t
        while True:

            # first iter
            kw = find_first_failed_child_keyword(kw)
            if kw == 0:
                # kw has no children
                if debug:
                    print "No children"
                break
            elif kw == None:
                # kw has children , but none on errors
                if debug:
                    print "No errors !!!!"
                break
            else:
                # we found a keyword on error
                yield 'KEYWORD' , kw
                #failed_keywords.append(kw)
            #print kw.attrib

        continue


def treat_test(test_element,keywords):
    """

    :param test_element:
    :param keyords:
    :return:
    """
    print ">> bad test :%s " % test_element.attrib['name']
    for kw in keywords:
        print ">>> bad keyword: %s" % kw.attrib['name']


    last_kw = keywords[-1]

    kw = last_kw.find('kw')

    sc = Screenshot(kw)

    print  ">>>> screenhot: %s" %  sc.snapshot

    return




if __name__ == "__main__":


    base = "../../samples/var/robot/results"

    o1 = "%s/%s" % (base,"OVC/150/Chrome32WINXP/output.xml")




    def test2():
        """

        :return:
        """

        x1 = ET.parse(o1)

        root = x1.getroot()




        last_test = None
        keywords=[]


        for kind, element  in scan_errors(root):
            if kind == "TEST":
                #print ">> bad test :%s " % element.attrib['name']

                if last_test:
                    # end of preceding test
                    treat_test(last_test,keywords)
                # save last test
                last_test = element
                keywords= []

            else:
                #print ">>> bad keyword: %s" % element.attrib['name']
                keywords.append(element)


        # treat last test
        treat_test(last_test,keywords)




        # tests = list(find_all_failed_test(root))
        #
        # for t in tests:
        #     print t.tag,t.attrib
        #
        #     failed_keywords = []
        #     kw = t
        #     while True:
        #
        #         # first iter
        #         kw = find_first_failed_child_keyword(kw)
        #         if kw == 0:
        #             # kw has no children
        #             print "No children"
        #             break
        #         elif kw == None:
        #             # kw has children , but none on errors
        #             print "No errors !!!!"
        #             break
        #         else:
        #             # we found a keyword on error
        #             failed_keywords.append(kw)
        #         print kw.attrib
        #
        #     continue







        return





    ####
    #test()
    test2()
