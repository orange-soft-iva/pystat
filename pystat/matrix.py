__author__ = 'cocoon'



class Table2D(object):
    """

        convenient class to handle 2D tables dict of dict

            'X.a':
                'Y.1': n1
                'Y.2': n2
            'X.b':
                'Y.1': n3
                'Y.2': n4

            eg
                with X.a= ModifyCompany , X.b = ModifyMeetingBooking
                with Y.1 = Chrome32_XP , Y.2 = IE7_XP


                ModifyCompany:
                    Chrome32_XP: 1
                    IE7_XP: 0
                ModifyMeetingBooking:
                    Chrome32_XP: 0
                    IE7_XP: 1

    """

    def __init__(self,labels=('x','y'),data=None,X=None,Y=None):
        """

        :return:
        """
        if not data:
            data ={}
        else:
            # check data
            assert isinstance(data,dict)
            for x,vx in data.iteritems():
                assert isinstance(vx,dict)
                for y,vy in vx.iteritems():
                    assert isinstance(vy,int)
        if not X:
            X={}
        if not Y:
            Y={}
        self._data = data
        self._X = X   # array of value for x axis
        self._Y = Y   # array of value for y axis
        self._labels = list(labels)  # tuple : labels for y and y axis


    def __repr__(self):
        return str(self._data)

    @property
    def data(self):
        return self._data

    @property
    def labels(self):
        return list(self._labels)


    def set(self,x,y,value):
        """

        :param x:
        :param y:
        :param value:
        :return:
        """
        if not x in self._data.keys():
            self._data[x]={}
        if not y in self._data[x].keys():
            self._data[x][y]=0
        self._data[x][y]=int(value)


    def get(self,x,y):
        """

        :param x:
        :param y:
        :return:
        """
        return self._data[x][y]

    def inc(self,x,y,value=1):
        """

        :param x:
        :param y:
        :return:
        """
        if not x in self._data.keys():
            self._data[x]={}
        if not y in self._data[x].keys():
            self._data[x][y]=0
        self._data[x][y] += value

    def iter(self):
        """
            iterate and yield a triplet x,y,value
        :return:
        """
        for x ,vx in self._data.iteritems():
            for y , vy in vx.iteritems():
                yield x , y ,vy


    def get_X(self):
        """
            get all values of x

        :return: list
        """
        return self._data.keys()

    def get_Y(self):
        """
            get all unique values of Y

        :return: list
        """
        res =set()
        for x,y,value in self.iter():
            res.add(y)
        return list(res)


    def minimize(self):
        """
            remove all entries with value 0
        :return:
        """
        # remove entries with 0 values
        to_delete=[]
        for x,y,value in self.iter():
            if value == 0:
                to_delete.append((x,y))
        for x,y in to_delete:
            del self._data[x][y]
        return self



    def invert(self):
        """
            compute the inverted matrix ( permute X , Y )
        :return:
        """
        inverted = Table2D(labels=reversed(self._labels))
        for x,y,value in self.iter():
            inverted.set(y,x,value)
        return inverted


    def copy(self):
        """
            copy the table
        :return: a new identical table
        """
        merged = Table2D(labels=self.labels)
        for x,y,v in self.iter():
            merged.set(x,y,v)
        return merged


    def merge(self,*tables):
        """
            create a table in merging tables
        """
        # copy the first matrix
        merged = self.copy()
        for added in tables:
            assert merged.labels == added.labels
            for x,y,v in added.iter():
                merged.inc(x,y,v)
        return merged


    def to_yaml(self,indent=0,spaces=4):
        """
            make formated yaml string

        :return:
        """
        space = " " * spaces

        i0 = space * indent
        i1 = space * (1+indent)

        lines=[]
        lines.append("%s# labels: %s" % (i0,str(self.labels)))
        last_x = None
        for x in sorted(self._data.keys()):
            if x != last_x:
                # new X
                lines.append("%s%s:" % (i0,x))
            last_x = x
            for y in sorted(self._data[x].keys()):
                lines.append("%s%s: %s" % (i1,y,self._data[x][y]))

        lines = "\n".join(lines)
        return lines


class Sparse(dict):
    """

    """

    def __add__(self,other):
        """

        :param other:
        :return:
        """
        res = Sparse(self.copy())
        for ij in other:
            res[ij] = self.get(ij,0) + other[ij]
        return res

if __name__=="__main__":


    def test():

        a = Table2D(labels=('test','env'))
        a.set('ModifyCompany','Chrome32_XP',0)
        a.set('ModifyCompany','IE7_XP',1)

        a.set('ModifyMeetingBooking','Chrome32_XP',1)
        a.set('ModifyMeetingBooking','IE7_XP',1)

        a.inc('ModifyMeetingBooking','IE7_XP')


        a.minimize()
        print a
        data = a.data

        print a.get_X()
        print a.get_Y()


        b = a.copy()

        c = a.merge(b)


        data_c = c.data

        d = Table2D(labels=c.labels,data=data_c)


        ia = a.invert()
        print ia.data

        yaml = ia.to_yaml(indent = 2)

        print yaml

        return


    def test_sparse():

        a = Sparse()

        a['modify','chrome'] = 1
        a['modify','ff'] = 1



        c = a + a


        return



    ########

    test_sparse()

    test()