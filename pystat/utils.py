__author__ = 'cocoon'
"""





"""
import os
import codecs
from composer import RobotImage


class Command(object):
    """


    """
    def __init__(self,args):
        """

        :param args:
        :return:
        """
        self.args =args
        self.debug=args['--trace']

    def arg(self,arg_name):
        """
        :param arg_name:
        :return:
        """
        return self.args[arg_name]

    def setup(self):
        """

        :return:
        """
        self.base_directory = os.path.abspath(self.arg('--base'))

        # check presence
        os.stat(self.base_directory)

        return


    def get_dir_for(self,job=None,run=None,env=None,check=False):
        """

        :param job:
        :param run:
        :param env:
        :return:
        """
        path = self.base_directory
        while 1:

            if job is None:
                break
            path=os.path.join(path,job)

            if run is None:
                break
            path=os.path.join(path,run)

            if env is None:
                break
            path=os.path.join(path,env)
            break
        if check:
            # check presence if requested
            os.stat(path)
        return path

    @property
    def job(self):
        """

        :return:
        """
        return self.arg('--job')

    @property
    def run(self):
        """

        :return:
        """
        return self.arg('--run')

    @property
    def env(self):
        """

        :return:
        """
        return self.arg('--env')

    def iter_dir(self,directory):
        """

        :return:
        """
        for f in os.listdir(directory):
            if not os.path.isdir(os.path.abspath(os.path.join(directory,f))):
                continue
            if f.startswith('.'):
                continue
            if f.startswith('_'):
                continue

            yield f



    # high level function

    def iter_jobs(self):
        """

            return the jobs , each job is a directory
        :return:
        """
        directory = self.base_directory
        return self.iter_dir(directory)

    def iter_runs(self,job=None):
        """

        :param job:
        :return:
        """
        if job is None:
            job = self.job
        directory= self.get_dir_for(job=job)
        return self.iter_dir(directory)

    def iter_envs(self,job=None,run=None):
        """

        :param job:
        :param run:
        :return:
        """
        if job is None:
            job=self.job
        if run is None:
            run=self.run
        directory= self.get_dir_for(job=job,run=run)
        return self.iter_dir(directory)

    def make_env(self,job,run,env):
        """
            make the xml for log_errors.xml , log_errors.html

            make the yaml for log_stat.yaml

        :return:
        """
        env_dir = self.get_dir_for(job,run,env)
        output = RobotImage(env_dir)

        # get xml string for lof_errors
        xml_errors = output.make(to_string=True, debug=self.debug)


        # get string for yaml_stat
        yaml_stat = output.stat(job,run,env)

        return xml_errors,yaml_stat

    def make_envs(self,job,run):
        """

            for each env of a run
                yield env_name , xml_errors , yaml_stat



        """
        for env in self.iter_envs(job=job,run=run):
            xml_errors , yaml_stat = self.make_env(job,run,env)
            yield env , xml_errors,yaml_stat







    def make_rebot(self,job,run,env):
        """
        """

        env_dir = self.get_dir_for(job,run,env)
        output = RobotImage(env_dir)

        output.rebot(directory=env_dir)

    def write_content_to_file(self,content,file=None,encoding='utf-8'):
        """

        """
        if file is None:
            print content
        else:

            #with open(file,"w") as fh:
            #    fh.write(content)

            with codecs.open(file,mode='w',encoding=encoding) as f:
                f.write(content)



if __name__== '__main__':

    def test():

        return

    ###
    test()